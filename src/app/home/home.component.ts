import { Component, OnInit } from '@angular/core';
import { cart, product } from '../data-type';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
 popularProducts:undefined|product[];
 trendyProducts:undefined | product[];
  constructor(private product:ProductService) {}

  ngOnInit(): void {
    this.product.popularProducts().subscribe((data)=>{
      this.popularProducts=data;
      // console.log(this.popularProducts);
      
    });

    this.product.trendyProducts().subscribe((data)=>{
      this.trendyProducts=data;
      // console.log(this.trendyProducts);
      
    });
  }
  addToCart(item: any){
    alert('Product added to cart');
    this.product.addFromThamb(item);
  }
}
